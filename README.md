# Automotive Toolchain container image for toolbx

[Automotive Toolchain] container image to use with [toolbx] ([GitHub]).

Toolbx image based on [CentOS Stream 9] with the most common tools
used by the Autotoolchain developers, such as:

- aws-cli
- black
- jq
- flake8
- make
- mdl (markdownlint)
- pip
- pre-commit
- pylint
- pytest
- Python3.9
- shellcheck
- shellspec
- tmt
- yamllint

The image has also [EPEL] enabled to be able to install packages not
included in [CentOS Stream 9].

This container image is based on the [community images].

## Usage

You can create the local toolbx container with the following commands:

```shell
$ toolbox create --image quay.io/automotive-toolchain/auto-toolbox
$ toolbox enter auto-toolbox
```

[Automotive Toolchain]: https://gitlab.com/redhat/edge/ci-cd
[toolbx]: https://containertoolbx.org
[GitHub]: https://github.com/containers/toolbox
[Quay.io]: https://quay.io/
[CentOS Stream 9]: https://www.centos.org/centos-stream/
[EPEL]: https://docs.fedoraproject.org/en-US/epel/
[community images]: https://github.com/toolbx-images/images
