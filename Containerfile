FROM quay.io/centos/centos:stream9

ENV NAME=auto-toolbox VERSION=stream9
LABEL com.github.containers.toolbox="true" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Automotive Toolbox toolbox containers based on CentOS Stream 9" \
      maintainer="Juanje Ojeda <juanje@redhat.com>"

ENV DNF_OPTS="--setopt=install_weak_deps=False --setopt=tsflags=nodocs"

COPY missing-docs extra-packages auto-packages requirements.txt /

RUN dnf -y swap coreutils-single coreutils-full && \
    dnf -y reinstall $(<missing-docs) && \
    dnf -y install 'dnf-command(config-manager)' && \
    dnf config-manager --set-enabled crb && \
    dnf -y install epel-release epel-next-release && \
    dnf -y install $(<extra-packages) $(<auto-packages) && \
    dnf -y upgrade && \
    dnf clean all

RUN pip install -U pip && \
    pip install --no-cache-dir -r requirements.txt

# Install hadolint
ARG HADOLINT_VER="v2.10.0"
ADD https://github.com/hadolint/hadolint/releases/download/${HADOLINT_VER}/hadolint-Linux-x86_64 \
    /usr/bin/hadolint

# Install shellspec
ARG SHELLSPEC_VER="0.28.1"
RUN git clone https://github.com/shellspec/shellspec.git /shellspec && \
    cd shellspec && \
    git checkout --detach ${SHELLSPEC_VER} && \
    make install

# Install markdownlint-cli
ARG MARKDOWNLINT_CLI_VER="v0.32.2"
RUN npm install -g markdownlint-cli@${MARKDOWNLINT_CLI_VER}

RUN rm -fr /missing-docs \
           /extra-packages \
           /auto-packages \
           /requirements.txt \
           /shellspec
